<?php

const MODE_SQLITE_TO_MYSQL = 1;
const MODE_MYSQL_TO_SQLITE = 2;

if (php_sapi_name() !== 'cli') {
    die('Sorry, only CLI mode support.' . PHP_EOL);
}

/*
 * Determine mode
 */
$usage_msg = 'Usage: php convert.php mode' . PHP_EOL
    . 'Modes:' . PHP_EOL
    . '1 - SQLite to MySQL' . PHP_EOL
    . '2 - MySQL to SQLite' . PHP_EOL;
if ($argc < 2 || !is_numeric($argv[1])) {
    die($usage_msg);
}
$possible_modes = [
    MODE_SQLITE_TO_MYSQL, MODE_MYSQL_TO_SQLITE
];
if (!in_array($argv[1], $possible_modes)) {
    die($usage_msg);
}
$mode = (int)$argv[1];

/*
 * Load configuration
 */
$cfgPath = __DIR__ . '/config/database.json';
if (!file_exists($cfgPath)) {
    die("Configuration file \"$cfgPath\" not found!\n");
}
$config = json_decode(
    file_get_contents($cfgPath)
);

/*
 * Connect to MySQL server
 */
$pdoMysql = new \PDO(
    "mysql:host={$config->mysql->host};dbname={$config->mysql->database}",
    $config->mysql->user, $config->mysql->password,
    [
        \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
        \PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES `utf8`',
    ]
);

/*
 * Open SQLite database
 */
$sqlite_path = __DIR__ . '/' . $config->sqlite->filename;
if (!file_exists($sqlite_path)) {
    die("SQLite database file not found, search path is \"$sqlite_path\"!\n");
}
$pdoSqlite = new \PDO(
    "sqlite:$sqlite_path",
    null,
    null,
    [
        \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
    ]
);

/*
 * Transfer data
 */
$tables = ['ZCLUSTER', 'ZPOINT'];
switch ($mode) {
    case MODE_SQLITE_TO_MYSQL:
        transfer_sqlite_to_mysql($tables, $pdoSqlite, $pdoMysql, $config->limit);
        break;
    case MODE_MYSQL_TO_SQLITE:
        transfer_mysql_to_sqlite($tables, $pdoSqlite, $pdoMysql, $config->limit);
        break;
}

/**
 * Transfer data from SQLite db to MySQL db
 *
 * @param array $tables
 * @param PDO $pdoSqlite
 * @param PDO $pdoMysql
 * @param int $limit
 */
function transfer_sqlite_to_mysql(array $tables, \PDO $pdoSqlite, \PDO $pdoMysql, $limit)
{
    echo 'Transfer data from SQLite to MySQL...' . PHP_EOL;

    echo 'Truncating tables: ' . implode(', ', $tables) . PHP_EOL;
    foreach ($tables as $tb) {
        $pdoMysql->exec("TRUNCATE TABLE `$tb`");
    }

    echo 'Transfer...' . PHP_EOL;
    foreach ($tables as $tb) {
        transfer_from_source_to_target($pdoSqlite, $pdoMysql, $limit, $tb);
    }
}

/**
 * Transfer data from MySQL db to SQLite db
 *
 * @param array $tables
 * @param PDO $pdoSqlite
 * @param PDO $pdoMysql
 * @param int $limit
 */
function transfer_mysql_to_sqlite(array $tables, \PDO $pdoSqlite, \PDO $pdoMysql, $limit)
{
    echo 'Transfer data from MySQL to SQLite...' . PHP_EOL;

    echo 'Truncating tables: ' . implode(', ', $tables) . PHP_EOL;
    foreach ($tables as $tb) {
        $pdoSqlite->exec("DELETE FROM `$tb`");
    }

    echo 'Transfer...' . PHP_EOL;
    foreach ($tables as $tb) {
        transfer_from_source_to_target($pdoMysql, $pdoSqlite, $limit, $tb);
    }
}

/**
 * Transfer data from source to target
 *
 * @param PDO $source
 * @param PDO $target
 * @param int $limit
 * @param string $table
 */
function transfer_from_source_to_target(\PDO $source, \PDO $target, $limit, $table) {

    echo 'Table: ' . $table . PHP_EOL;

    $offset = 0;
    for (; ;) {

        echo "Reading from source, offset = $offset, limit = $limit\n";
        $sql = "SELECT * FROM `$table` LIMIT $offset, $limit";
        $statement = $source->query($sql);
        $rows = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $count = count($rows);
        echo $count . ' row(s) from source...' . PHP_EOL;
        if ($count == 0) {
            echo 'Empty result, operation done.' . PHP_EOL;
            break;
        }

        echo 'Writing to target...' . PHP_EOL;
        foreach ($rows as $row) {
            $statement = convertRowToStatement($target, $row, $table);
            $statement->execute();
        }

        $offset += $limit;
    }
}

/**
 * Convert selection result row to a insert statement
 *
 * @param PDO $pdo
 * @param array $row
 * @param string $table
 * @return PDOStatement
 */
function convertRowToStatement(\PDO $pdo, array $row, $table)
{
    // Prepare SQL
    $keys = array_keys($row);

    $sql = "INSERT INTO `$table` (" . implode(', ', $keys) . ') VALUES (';

    $placeholders = [];
    foreach ($keys as $k) {
        $placeholders[] = ":$k";
    }

    $sql .= implode(', ', $placeholders) . ')';

    // Prepare statement
    $statement = $pdo->prepare($sql);
    foreach ($keys as $k) {
        $statement->bindValue(":$k", $row[$k]);
    }

    return $statement;
}

