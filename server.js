var
    http = require("http"),
    mysql = require("mysql"),
    path = require("path"),
    fs = require("fs"),
    url = require("url"),
    moment = require("moment"),
    crypto = require("crypto"),
    colors = require("colors");

console.log("Hello!");

/*
 Load configuration
 */
var cfg_db = loadConfiguration(
    path.resolve(__dirname, "config", "database.json")
);
var cfg_app = loadConfiguration(
    path.resolve(__dirname, "config", "server.json")
);
var package_info = loadConfiguration(
    path.resolve(__dirname, "package.json")
);
console.log("Points-Server v." + package_info.version);

/*
 Load params map
 */
var params_map = {};
params_map.show_points = loadConfiguration(
    path.resolve(__dirname, "config", "params_map", "show_points.json")
);
params_map.show_clusters = loadConfiguration(
    path.resolve(__dirname, "config", "params_map", "show_clusters.json")
);
params_map.create_point = loadConfiguration(
    path.resolve(__dirname, "config", "params_map", "create_point.json")
);

/*
 Connect to MySQL server
 */
var connection_mysql = mysql.createConnection(cfg_db.mysql);
connection_mysql.connect(function (err) {

    var msg = null;
    if (err && err.fatal) {
        msg = "Can`t open connection to the MySQL server, error code: " + err.code;
        console.error(msg.red);
        connection_mysql.end();
        return;
    }

    /*
     Bind server
     */
    msg = "Binding port: " + cfg_app.bind_port;
    console.log(msg.green);
    http.createServer(function (req, res) {
        processRequest(req, res);
    }).listen(cfg_app.bind_port);

});

function processMySQLConnectionErrorAndCallACallback(err, callback) {

}

/**
 *
 * @param req
 * @param res
 */
function processRequest(req, res) {

    var query = url.parse(req.url, true).query;

    var what = null;
    if (query.hasOwnProperty("cluster") && !query.hasOwnProperty("act")) {
        what = "cluster";
    } else if (query.hasOwnProperty("what")) {
        what = query.what;
    } else {
        what = "point";
    }
    var supportedEntities = [
        "point", "cluster"
    ];
    if (supportedEntities.indexOf(what) == -1) {
        sendJSONObject(res, 400, {
            "err": "Unknown entity type"
        });
        return;
    }

    switch (what) {
        case "point":
            processPointsRequest(query, res);
            break;
        case "cluster":
            processClustersRequest(query, res);
            break;
    }
}

/**
 *
 * @param query
 * @param res
 */
function processPointsRequest(query, res) {
    var action = query.hasOwnProperty("act") ? query.act : "show";
    var supportedActions = [
        "show", "create"
    ];
    if (supportedActions.indexOf(action) == -1) {
        sendJSONObject(res, 400, {
            "err": "invalid_args"
        });
        return;
    }

    switch (action) {
        case "show":
            showPoints(query, res);
            break;
        case "create":
            createPoint(query, res);
            break;
    }
}

/**
 *
 * @param query
 * @param res
 */
function showPoints(query, res) {
    /*
     Check required params are present
     */
    var requiredParams = [
        "lat1", "lat2", "lng1", "lng2"
    ];
    if (!checkRequiredParams(res, query, requiredParams)) {
        return;
    }

    // Get page number and check it has a numeric value
    var page = getPageValue(query);
    if (isNaN(page)) {
        return;
    }

    /*
     Get date to search from
     */
    var inserted = getInsertedValue(query);
    if (inserted === false) {
        return;
    }

    // Get limit from app config
    var limit = cfg_app.points_limit;

    // Build SQL query to select points
    var offset = page * limit;
    var values = [query.lat1, query.lat2, query.lng1, query.lng2];
    var sql = "SELECT * FROM `ZPOINT` WHERE (`ZLAT` BETWEEN ? AND ?) AND (`ZLON` BETWEEN ? AND ?) ";
    if (inserted !== null) {
        sql += "AND `ZCREATEDATE` >= ? ";
        values.push(inserted);
    }
    sql += "LIMIT " + offset + ", " + limit;
    connection_mysql.query(sql, values, function (err, results) {
        if (err) throw err;
        processResultSet(res, results, params_map.show_points);
    });
}

/**
 *
 * @param query
 * @param res
 */
function createPoint(query, res) {
    // Check required params
    var requiredParams = [
        "lat", "lng", "cluster", "title"
    ];
    if (!checkRequiredParams(res, query, requiredParams)) {
        return;
    }

    // Get new point`s data
    var point = prepareItem(params_map.create_point, query);

    // Check point already exists
    var sql = "SELECT COUNT(*) AS `count` FROM `ZPOINT` WHERE `ZLAT` = ? AND `ZLON` = ?";
    connection_mysql.query(sql, [point.ZLAT, point.ZLON], function (err, results) {

        if (err) throw err;

        // If point already exists...
        if (results[0].count > 0) {
            sendJSONObject(res, 400, {"err": "exists", "lat": point.ZLAT, "lon": point.ZLON});
            return;
        }

        // Check cluster exists
        sql = "SELECT COUNT(*) AS `count` FROM `ZCLUSTER` WHERE `Z_PK` = ?";
        connection_mysql.query(sql, [point.ZCLUSTER], function (err, results) {

            if (err) throw err;

            // If cluster not found...
            if (results[0].count == 0) {
                sendJSONObject(res, 404, {"err": "cluster"});
                return;
            }

            // Generate point identifier
            point["ZIDENTIFIER"] = crypto.createHash("sha1").update(JSON.stringify(point) + moment().format("X")).digest("hex");

            // Insert point to database
            sql = "INSERT INTO `ZPOINT` ("
                + "`Z_PK`, `Z_ENT`, `Z_OPT`, `ZISOPEN`, `ZCLUSTER`, `ZCREATEDATE`, `ZLAT`, `ZLON`, `ZUPDATEDATE`, "
                + "`ZADDRESS`, `ZCOMMENT`, `ZIDENTIFIER`, `ZTITLE`"
                + ") VALUES ("
                + "NULL, 2, 1, 0, ?, NOW(), ?, ?, NOW(), ?, ?, ?, ?"
                + ")";
            connection_mysql.query(
                sql,
                [
                    point.ZCLUSTER, point.ZLAT, point.ZLON,
                    point.ZADDRESS, point.ZCOMMENT, point.ZIDENTIFIER,
                    point.ZTITLE
                ],
                function (err, result) {
                    if (err) throw err;
                    sendJSONObject(res, 200, {"auto": result.insertId, "id": point.ZIDENTIFIER});
                }
            );
        });

    });
}

/**
 *
 * @param query
 * @param res
 */
function processClustersRequest(query, res) {
    if (query.hasOwnProperty("cluster")) {
        showPointsByClustersIds(query, res);
    } else {
        showClusters(query, res);
    }
}

/**
 *
 * @param query
 * @param res
 */
function showClusters(query, res) {

    /*
     Check required params are present
     */
    var requiredParams = [
        "lat1", "lat2", "lng1", "lng2"
    ];
    if (!checkRequiredParams(res, query, requiredParams)) {
        return;
    }

    // Get page number and check it has a numeric value
    var page = getPageValue(query);
    if (isNaN(page)) {
        return;
    }

    // Get limit from app config
    var limit = cfg_app.clusters_limit;

    /*
     Get date to search from
     */
    var inserted = getInsertedValue(query);
    if (inserted === false) {
        return;
    }

    // Build SQL query to select clusters
    var offset = page * limit;
    var values = [query.lat1, query.lat2, query.lng1, query.lng2];
    var sql = "SELECT * FROM `ZCLUSTER` WHERE (`ZLAT` BETWEEN ? AND ?) AND (`ZLON` BETWEEN ? AND ?) ";
    if (inserted !== null) {
        sql += "AND `ZCREATEDATE` >= ? ";
        values.push(inserted);
    }
    sql += "LIMIT " + offset + ", " + limit;
    connection_mysql.query(sql, values, function (err, results) {
        if (err) throw err;
        processResultSet(res, results, params_map.show_clusters);
    });
}

/**
 *
 * @param query
 * @param res
 */
function showPointsByClustersIds(query, res) {
    // Get clusters ids
    var ids = query.cluster.trim();
    if (ids.length == 0) {
        sendJSONObject(res, 400, {
            "err": "invalid_args"
        });
        return;
    }

    // Check clusters ids
    ids = ids.split(",");
    for (var i = 0; i < ids.length; ++i) {
        var num = parseInt(ids[i]);
        if (isNaN(num)) {
            sendJSONObject(res, 400, {
                "err": "invalid_args"
            });
            return;
        }
        ids[i] = num;
    }

    // Get page number and check it has a numeric value
    var page = getPageValue(query);
    if (isNaN(page)) {
        return;
    }

    // Get limit from app config
    var limit = cfg_app.points_limit;

    var offset = page * limit;
    var sql = "SELECT * FROM `ZPOINT` WHERE `Z_PK` IN (?) LIMIT " + offset + ", " + limit;
    connection_mysql.query(sql, [ids], function (err, results) {
        if (err) throw err;
        processResultSet(res, results, params_map.show_points);
    });
}

/**
 *
 * @param res
 * @param httpCode
 * @param object
 */
function sendJSONObject(res, httpCode, object) {
    res.writeHead(httpCode, {"Content-Type": "application/json; charset=utf-8"});
    res.end(JSON.stringify(object));
}

/**
 *
 * @param path
 * @returns {*}
 */
function loadConfiguration(path) {
    var msg = "Loading configuration file \"" + path + "\"...";
    console.log(msg.green);
    if (!fs.existsSync(path)) {
        msg = "File \"" + cfg_path_db + "\" not found!";
        console.error(msg.red);
        process.exit(1);
    }
    return require(path);
}

/**
 *
 * @param query
 * @returns {*}
 */
function getPageValue(query) {
    // Get page number and check it has a numeric value
    var page = query.hasOwnProperty("page") ? parseInt(query.page) : 0;
    if (isNaN(page)) {
        sendJSONObject(res, 400, {
            "err": "invalid_args"
        });
        return NaN;
    }
    return page > 0 ? page - 1 : page;
}

/**
 *
 * @param query
 * @returns {*}
 */
function getInsertedValue(query) {
    var inserted = query.hasOwnProperty("inserted") ? query.inserted : null;
    if (inserted !== null) {
        var mDt = moment(inserted, "YYYY.MM.DD");
        if (!mDt.isValid()) {
            sendJSONObject(res, 400, {
                "err": "invalid_args"
            });
            return false;
        }
        inserted = mDt.format("YYYY-MM-DD");
        mDt = null;
    }
    return inserted;
}

/**
 *
 * @param res
 * @param results
 * @param map
 */
function processResultSet(res, results, map) {
    var items = [];
    results.forEach(function (row) {
        row = prepareItem(map, row);
        items.push(row);
    });

    sendJSONObject(res, 200, items);
}

/**
 *
 * @param map
 * @param item
 * @returns {{}}
 */
function prepareItem(map, item) {

    var result = {};

    for (var key in map) {

        if (!map.hasOwnProperty(key)) {
            continue;
        }

        var mapItem = map[key];
        switch (mapItem.type) {
            case "fixed":
                result[key] = mapItem.value;
                break;
            case "list":
                if (!item.hasOwnProperty(mapItem.property)) {
                    result[key] = [];
                } else {
                    var raw = item[mapItem.property];
                    result[key] = raw.split(mapItem.delimiter);
                }
                break;
            case "value":
                if (!item.hasOwnProperty(mapItem.property)) {
                    result[key] = null;
                } else {
                    result[key] = item[mapItem.property];
                    if (mapItem.property == "ZCREATEDATE" || mapItem.property == "ZUPDATEDATE") {
                        var mDt = moment(result[key]);
                        result[key] = mDt.format("YYYY.MM.DD HH:mm:ss");
                        mDt = null;
                    }
                }
                break;
        }
    }

    return result;
}

/**
 *
 * @param res
 * @param query
 * @param requiredParams
 * @returns {boolean}
 */
function checkRequiredParams(res, query, requiredParams) {

    var numericParams = [
        "lat", "lng", "lat1", "lat1",
        "lng1", "lng2", "cluster"
    ];

    for (var i = 0; i < requiredParams.length; ++i) {
        var paramName = requiredParams[i];
        if (query.hasOwnProperty(paramName)) {

            if (numericParams.indexOf(paramName) != -1) {
                query[paramName] = parseFloat(query[paramName]);
                if (isNaN(query[paramName])) {
                    sendJSONObject(res, 400, {
                        "err": "invalid_args"
                    });
                    return false;
                }
            }

            continue;
        }
        sendJSONObject(res, 400, {
            "err": "invalid_args"
        });
        return false;
    }

    return true;
}